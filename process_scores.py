class Grader:
   #dictionary of the students names with their scores 

  #takes in the list of students resposne and correct answer string
  #initialize the empty lists
  def __init__(self,students,correct_key):
    self.__students = students
    self.__correct_key = correct_key 
    self.__corrected_scores = []
    self.__sorted_scores = [] 

  def compute_grade(self,score):
    letter=''
    if(score > 90 and score <= 100):
      letter='A'
    elif(score >= 80 and score <= 89):
      letter='B'
    elif(score >= 70 and score <= 79):
      letter='C'
    elif(score >= 60 and score <= 69):
      letter='D'
    elif(score >= 50 and score <= 59):
      letter='E'
    elif(score >= 0 and score <= 50):
      letter='F'
    return letter

  #return the 0
  def compute_score(self,dict):
    return 0
  
  #grading students with internal method 
  def correct_students(self):
    return 
  
  def get_corrected_scores(self):
    return self.__corrected_scores

  def get_sorted_scores(self):
    return self.__sorted_scores